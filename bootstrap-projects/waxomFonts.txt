waxom fonts


montserrat light


raleway medium
raleway extra bold
raleway semi bold
raleway regular
montserrat ultralight

font sizes:
header items: 14px;

hero section:
intro heading: 36px
heading: 52px
description: 16px
button: 14px;

services:
title: 22px
content:14px

realization:
title: 30px
content: 16px

WP theme:
title: 28px;
content: 18px;
button: 14px;

projects:
title: 30px;
sub heading: 16px;
tabs: 14px;
-project square:
title: 20px;
sub heading: 14px;
button: 14px

video presentation:
title: 30px;
sub heading: 16px
time: 14px;

excellent for mobile
title: 30px;
description: 16px;
list: 16px;

counter:
count: 48px;
description: 14px;

recent posts:
title: 30px;
content: 16px;
-posts square:
date num: 24px;
month: 12px;
title: 20px;
sub heading: 14px;
read more: 14px;

footer section 1:
waxom:
content: 14px;
read more: 14px;

recent posts:
title: 24px;
date: 12px;
content: 14px;

our twitter:
title: 24px;
content: 14px;
hours: 12px;

dribble widget:
title: 24px;

footer section 2:
content: 13px;


