/*
    Author: Carlo Canlas
    File name: offCanvas.js
    Description: script for off-canvas menu on mobile
    Dependencies: jQuery
*/

(function($) {
	"use strict";

	var offCanvas = {
		init: function() {
			this.cache();
			this.showCanvas();
			this.disableCanvas();
		},

		// store inner wrapper class
		cache: function() {
			this.$innerWrap = $('.inner-wrap');
		},

		//Toggle menu on click event of hamburger icon
		showCanvas: function() {
			var dom = offCanvas;

			$('.mobile-menu').on('click', function() {
    			$(this).toggleClass('open');
    			dom.$innerWrap.toggleClass('active');
    		});
		},

		//Disable off canvas menu when window is resized larger than mobile size
		disableCanvas: function() {
			var dom = offCanvas;

			$(window).on('resize', function() {
    			if ( $(this).width() > 768 && $(dom.$innerWrap).hasClass('active') ) {
    				$(dom.$innerWrap).removeClass('active')
    			}
    		});
		}
	}

	// initialize the function
	$(function() {
		offCanvas.init();
	});

})(jQuery);