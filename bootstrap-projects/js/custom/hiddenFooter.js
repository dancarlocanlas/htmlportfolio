/*
    Author: Carlo Canlas
    File name: hiddenFooter.js
    Description: script to calculate footer height and add 
    			 the vlaue as magin of content container
    			 this is currently only used in desktop and disabled on mobile
    Dependencies: jQuery
*/

(function($) {
	"use strict";

	var hiddenFooter = {
		init: function() {
			this.footerShow();
		},

		footerShow: function() {
			//store dom elements
			var	$content = $('.main-content'),
				footerHeight = $('.footer-hidden').outerHeight(true);

			$(window).on('resize', function() {
				//check window width to determine if margin for content container is needed
				if( $(this).width() < 768 ) {

					// set container margin bottom to 0 on mobile since footer is "position: relative"
					$content.css( 'margin-bottom', '0');
				} else {
					
					//set container margin bottom to height of footer
					$content.css ('margin-bottom', footerHeight);
				}
			}).resize(); //trigger resize event to load script when page loads
		}
	}

	// initialize the function
	$(function() {
		hiddenFooter.init();
	});

})(jQuery);