/*
    Author: Carlo Canlas
    File name: waxom.js
    Description: custom scripts used for waxom template
    Dependencies: jQuery
*/

(function($) {
	"use strict";

	var waxom = {
		init: function() {
			this.headerScrollbackground();
			this.mobileTabs();
		},

		// Add background to header when scrolled to content
		headerScrollbackground: function() {
			var $header = $('#waxom header');
			
			$(window).on('scroll', function() {
    			if($(this).scrollTop() > 80) {
    				$header.addClass('active');
    			} else {
    				$header.removeClass('active');
    			}
    		});
		},

		//slideDown tab options on mobile
		mobileTabs: function() {
			$('.tab-drop').on('click', function(e) {
    			e.preventDefault();
    			$('.nav-tabs').slideToggle();
    		});
		}
	}

	// initialize the function
	$(function() {
		waxom.init();
	});

})(jQuery);